package edu.melbu.shinypandas.game;

import org.json.simple.JSONObject;

public class NewPandaInstruction implements Instruction {

	Panda panda;
	
	public NewPandaInstruction(Panda panda) {
		this.panda = panda; 
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public String getJSON() {
		JSONObject obj = new JSONObject();
		
		obj.put("panda_id", panda.getID());
		obj.put("panda_pos_x", panda.getPosition().getX());
		obj.put("panda_pos_y", panda.getPosition().getY());
		obj.put("panda_dir_x", panda.getDirection().getX());
		obj.put("panda_dir_y", panda.getDirection().getY());
		
		return obj.toJSONString();
	}
}
