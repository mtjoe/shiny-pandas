package edu.melbu.shinypandas.game;

import java.util.ArrayList;

import edu.melbu.shinypandas.activities.ARActivity;

/**
 * This class store the game data such as number of players, scores for each of
 * the player and information the game need.
 */
public class Game {
	private ARActivity activity;
	private int nPlayers;
	private int[] scores;

	// Array list containing panda objects
	private ArrayList<Panda> pandas;

	// Magic numbers
	private static final int MAXPANDA = 3;
	private final int SCOREADDITION = 10;
	private final int SCOREMINUS = 10;
	private int pandaIdCounter;

	public Game(ARActivity activity, int nPlayers) {
		this.activity = activity;
		this.nPlayers = nPlayers;
		this.pandaIdCounter = 0;

		// Initiliaze scores to 0 for all players
		this.scores = new int[this.nPlayers];
		for (int i = 0; i < this.nPlayers; i++) {
			scores[i] = 0;
		}

		pandas = new ArrayList<Panda>();
	}

	/**
	 * Initialize the game state, creating pandas.
	 */
	public void initializeState() {
		addPanda(MAXPANDA);
	}

	/**
	 * Rotate panda by index in the list
	 * 
	 * @param index
	 *            - index that identify the panda in the list
	 */
	public void rotatePanda(int index) {
		Panda panda = pandas.get(index);
		panda.rotate();
		// Create instruction for other player
		Instruction instr = new PlayerActionInstruction(panda.getID());
	}

	/**
	 * Adding pandas to the game state until it reaches the upper limit of
	 * number of pandas. We are trying to keep the number of active pandas to be
	 * constant.
	 * 
	 * @param n
	 *            - Number of pandas to be added
	 */
	public void addPanda(int n) {
		Panda panda;
		Instruction instr;
		for (int i=0; i< n; i++) {
			panda = new Panda(pandaIdCounter);
			this.pandas.add(panda);
			this.activity.addPanda(panda);
			this.pandaIdCounter++;
			
			// Create instruction for other player
			instr = new NewPandaInstruction(panda);
		}
	}

	/**
	 * Add score to player pIndex
	 *
	 * @param pIndex
	 *            - player index
	 */
	public void addScore(int pIndex) {
		scores[pIndex] += SCOREADDITION;
	}

	/**
	 * Deduct score from player pIndex
	 * 
	 * @param pIndex
	 *            - player index
	 */
	public void decScore(int pIndex) {
		scores[pIndex] -= SCOREMINUS;
	}

	/**
	 * Function will be called when panda walk into the cage or walk outside the
	 * boundary
	 * 
	 * @param index
	 *            - the index of panda in the list
	 */
	public void removePanda(int index) {
		pandas.remove(index);
	}

	/**
	 * Add panda to keep a constant number of panda
	 */
	public void refreshPandas() {
		addPanda(MAXPANDA - pandas.size());
	}

	/**
	 * GETTER METHODS
	 */

	/**
	 * Get the panda whose index in the array is that of the provided index
	 * 
	 * @param index
	 *            - the index of the panda that needs to be retrieved
	 * @return the panda with provided index
	 */
	public Panda getPanda(int index) {
		return pandas.get(index);
	}

	/**
	 * Get score of player whose index in the array is that of the provided
	 * index
	 * 
	 * @param index
	 *            - the index of the player that whose score needs to be
	 *            retrieved
	 * @return the score of the player with the provided index
	 */
	public int getScore(int index) {
		return scores[index];
	}
}
