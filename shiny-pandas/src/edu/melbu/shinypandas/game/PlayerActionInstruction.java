package edu.melbu.shinypandas.game;

import org.json.simple.JSONObject;

public class PlayerActionInstruction implements Instruction {
	public int pandaId;

	public PlayerActionInstruction(int pandaId) {
		this.pandaId = pandaId;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public String getJSON() {
		JSONObject obj = new JSONObject();
		
		obj.put("pandaId", pandaId);
		
		return obj.toJSONString();
	}
}
