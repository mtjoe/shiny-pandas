package edu.melbu.shinypandas.game;

import com.metaio.sdk.jni.Vector3d;



/**
 * This class store the functions and property of a Panda object
 *  
 * @author leungtimothy, marisa tjoe
 *
 */
public class Panda {
	private int id;
	private Vector3d position;
	private Vector3d direction;
	private int SPEED_FACTOR = 3;

	private final static int BOUNDARY_LIMIT = 480;
	private final static int ORIGIN_LIMIT = 50;
	/**
	 * Constructor for Panda
	 * position and direction are initialize here, position are set
	 * near the middle of the cage and boundary. 
	 */
	public Panda(int pandaId){
		this.id = pandaId;
		position = new Vector3d(getRandom() * BOUNDARY_LIMIT/2, getRandom() * BOUNDARY_LIMIT/2, 0);
		direction = new Vector3d(getRandom() * BOUNDARY_LIMIT/2, getRandom() * BOUNDARY_LIMIT/2, 0).normalize();
	}
	
	public Panda(int id, Vector3d position, Vector3d direction) {
		this.id = id;
		this.position = position;
		this.direction = direction;
	}
	
	public void setVector(float x, float y, float z){
		this.position = new Vector3d(x, y, z);
	}
	
	public void setVector(Vector3d vector){
		this.position = vector;
	}
	
	public int getID(){
		return id;
	}
	
	public void setID(int id) {
		this.id = id;
	}

	/**
	 * Rotate the object by 90 degree
	 */
	public void rotate() {
		direction = direction.cross(new Vector3d(0, 0, 1));
	}
	
	/**
	 * Get the x direction of the 
	 * @return
	 */
	public Vector3d getDirection() {
		return direction;
	}
	
	public Vector3d getPosition() {
		return this.position;
	}
	
	public Vector3d getAndSetNextPosition() {
		this.position = this.position.add(this.direction.multiply(SPEED_FACTOR));
		return this.position;
	}
	
	/**
	 * Check the position of panda near the origin or not. 
	 * If it is in certain range near the origin, it will return true
	 * else return false
	 * @return boolean value to indicate whether the panda near the origin 
	 * 		   or not
	 */
	public boolean isNearOrigin() {
		float x = this.position.getX();
		float y = this.position.getY();
		float distance = (float) Math.sqrt(x*x + y*y);
		
		return distance < ORIGIN_LIMIT;
	}
	
	/**
	 * Check the position of panda outside the boundary or not. 
	 * Function return true if panda is outside the boundary, else
	 * return false
	 * @return
	 */
	public boolean isOutsideBoundary() {
		float x = this.position.getX();
		float y = this.position.getY();
		float distance = (float) Math.sqrt(x*x + y*y);
		return distance > BOUNDARY_LIMIT;
	}
	
	public float getRandom() {
		double random = (Math.random() - 0.5) * 2;
		while(random == 0) {
			random = (Math.random() - 0.5) * 2;
		}
		return (float)random;
	}
}
