package edu.melbu.shinypandas.game;

import java.util.concurrent.TimeUnit;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.os.Build;
import android.os.CountDownTimer;
import android.widget.TextView;

@TargetApi(Build.VERSION_CODES.GINGERBREAD)
@SuppressLint("NewApi")
public class Counter extends CountDownTimer {

	public TextView textViewTime;
	private long millis;
	public Counter(long millisInFuture, long countDownInterval, TextView textViewTime) {
		super(millisInFuture, countDownInterval);
		this.textViewTime = textViewTime;
	}

	@SuppressLint("NewApi")
	@TargetApi(Build.VERSION_CODES.GINGERBREAD)
	@Override
	public void onTick(long millisUntilFinished) {		
		millis = millisUntilFinished;
		String hms = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millis),
				TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
				TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
		System.out.println(hms);
		textViewTime.setText(hms);
		
	}

	public long getTimeLeft(){
		return this.millis;
	}
	
	@Override
	public void onFinish() {
		// TODO Auto-generated method stub
		textViewTime.setText("Time Up!");
	}
	
	
	
	
	
}
