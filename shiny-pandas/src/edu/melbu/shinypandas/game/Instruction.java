package edu.melbu.shinypandas.game;


public interface Instruction {

	/**
	 * Instructions will be send from one player to the other in a JSON format.
	 * The method getJSON() will return the JSON formatted string of the
	 * instruction.
	 * 
	 * @return JSON formatted string of the instruction
	 */
	public String getJSON();
}
