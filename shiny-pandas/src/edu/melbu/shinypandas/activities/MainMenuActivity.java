package edu.melbu.shinypandas.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;
import edu.melbu.shinypandas.R;

/**
 * Contains main menu, with a button linking it to the choose game page, how to
 * play page, and about us page.
 */
public class MainMenuActivity extends ActionBarActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main_menu);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main_menu, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * Called when 'New Game' button clicked, will link to ChooseGameActivity
	 * 
	 * @param view
	 *            
	 */
	public void newGame(View view) {
		Intent intent = new Intent(getApplicationContext(),
				edu.melbu.shinypandas.activities.ARActivity.class);
		startActivity(intent);
	}

	/**
	 * Called when 'How to Play' button clicked, will link to HowtoPlayActivity
	 * 
	 * @param view
	 */
	public void howToPlay(View view) {

		Intent intent = new Intent(this,
				edu.melbu.shinypandas.activities.HowToActivity.class);
		startActivity(intent);
	}

	/**
	 * Called when 'About Us' button clicked, will link to AboutUsActivity
	 * 
	 * @param view
	 */
	public void aboutUs(View view) {
		Intent intent = new Intent(this,
				edu.melbu.shinypandas.activities.AboutUsActivity.class);
		startActivity(intent);
	}
	
	/**
	 * Test screen to start two player game. Will be embedded into new 
	 * game later.
	 * 
	 * @param view
	 */
	
	public void multiplayerGame(View view) {
		Intent intent = new Intent(this,
				edu.melbu.shinypandas.activities.MultiplayerConnectActivity.class);
		startActivity(intent);
	}
}
