package edu.melbu.shinypandas.activities;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.metaio.sdk.ARViewActivity;
import com.metaio.sdk.MetaioDebug;
import com.metaio.sdk.jni.IGeometry;
import com.metaio.sdk.jni.IMetaioSDKCallback;
import com.metaio.sdk.jni.Rotation;
import com.metaio.sdk.jni.Vector3d;
import com.metaio.tools.io.AssetsManager;

import edu.melbu.shinypandas.R;
import edu.melbu.shinypandas.R.string;
import edu.melbu.shinypandas.game.Counter;
import edu.melbu.shinypandas.game.Game;
import edu.melbu.shinypandas.game.Panda;

/**
 * This class is where we use MetaioSDK to create an AR view, render 3D objects
 * on screen, and the main activity where the game is conducted in
 */
public class ARActivity extends ARViewActivity {
	private String mTrackingFile;
	
	public Game game;
	private MetaioSDKCallbackHandler mCallbackHandler;

	// Paths to the directory containing the respective models
	String PandaModelPath;
	String CageModelPath;
	String BoundaryModelPath;

	// List containing iGeometry objects of pandas, index is same to index in
	// Game.pandas
	private List<IGeometry> pandas_geometry;

	/* view to display score, timer and pause Button */
	TextView scoreView;
	TextView timerTextView; 
	ImageButton pauseButton;
	
	/* view and layout used to display menu when the game is end */
	LinearLayout endGameMenuLayout;
	TextView endGameMessageView;
	Button backToMenuButton;

	
	Panda panda;
	IGeometry pandaGeo;
	
	/* timer and flag for game pause */
	private Counter counter;
	boolean gamePausedFlag;
	long timeLeft;

	/* CONSTANT */
	final static float CAGE_SCALE = 40;
	final static float BOUNDARY_SCALE = 100;
	final static float PANDE_SCALE = 4;
	final static long TIME_LIMIT = 20000;
	final static long TIME_UNIT = 1000; 
	
	@SuppressWarnings("deprecation")
	public ARActivity() throws Exception {
		super();

		// Initialize Cage Model Path
		CageModelPath = AssetsManager.getAssetPath("Cage/iron_cage.obj");
		if (CageModelPath == null) {
			MetaioDebug.log("Cant find the cage.. ");
			throw new Exception();
		}

		// Initialize Panda Model Path
		PandaModelPath = AssetsManager.getAssetPath("Assets1/sailboat.zip");
		if (PandaModelPath == null) {
			MetaioDebug.log("Cant find the panda.. ");
			throw new Exception();
		}

		BoundaryModelPath = AssetsManager.getAssetPath("Boundary/circle.obj");
		if (BoundaryModelPath == null) {
			MetaioDebug.log("Cant find the boundary.. ");
			throw new Exception();
		}
		// Initialize Attributes
		pandas_geometry = new ArrayList<IGeometry>();
		
		gamePausedFlag = false;
		
		// Initialize game object
		game = new Game(this, 1);
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mCallbackHandler = new MetaioSDKCallbackHandler();
	}
	
	/*
	 * Called when the pause button is being clicked 
	 * 
	 */
	public void onPauseButtonClick(View v){
		if(gamePausedFlag){
			// gamePaused.. turn it on now.. 
			Toast.makeText(ARActivity.this, "Game resume!", Toast.LENGTH_SHORT).show();
			gamePausedFlag = false;
			pauseButton.setImageResource(R.drawable.pause_btn); // change a pause button picture
			// restart timer here
			counter = new Counter(timeLeft, 1000, timerTextView);
			counter.start();
		} else {
			// stop the timer
			timeLeft = counter.getTimeLeft();
			counter.cancel();
			gamePausedFlag = true;
			pauseButton.setImageResource(R.drawable.play_btn);
			Toast.makeText(ARActivity.this, "Game paused", Toast.LENGTH_SHORT).show();
		}
	}
	
	@Override
	protected void onDestroy() 
	{
		super.onDestroy();
		mCallbackHandler.delete();
		mCallbackHandler = null;
	}
	
	@Override
	protected int getGUILayout() {
		return R.layout.ar_view;
	}


	@SuppressWarnings("deprecation")
	@Override
	protected void loadContents() {
		// (1) Load the tracking file
		mTrackingFile = AssetsManager
				.getAssetPath("Assets1/TrackingData_MarkerlessFast.xml");
		boolean result = metaioSDK.setTrackingConfiguration(mTrackingFile);
		MetaioDebug.log("Tracking Data " + result);

		// (2) Render cage on the screen
		IGeometry mCage = metaioSDK.createGeometry(CageModelPath);
		if (mCage != null) {
			mCage.setScale(new Vector3d(CAGE_SCALE, CAGE_SCALE, CAGE_SCALE));
			mCage.setRotation(new Rotation((float)Math.PI/2, 0.0f, 0.0f));
			mCage.setVisible(true);
			MetaioDebug.log("mCage loaded!");
		} else {
			MetaioDebug.log("Error in loadding mCage");
		}
		
		// Render boudnary around the cage
		IGeometry mBoundary = metaioSDK.createGeometry(BoundaryModelPath);
		if (mBoundary != null) {
			mBoundary.setScale(new Vector3d(BOUNDARY_SCALE, BOUNDARY_SCALE, BOUNDARY_SCALE));
			mBoundary.setRotation(new Rotation((float) Math.PI / 2, 0.0f, 0.0f));
			mBoundary.setVisible(true);
			MetaioDebug.log("mBoundary loaded!");
		}
		game.initializeState();
	}


	@SuppressLint("NewApi")
	@Override
	public void onDrawFrame() {
		metaioSDK.render();
		if (checkGameEnd()){
			return;
		}
		if(gamePausedFlag){
			return;
		}
		
		if (pandas_geometry.size() != 0) {
			// Translation of each pandas
			try {
				// Loop through each panda, and set new translation value
				for (int i = 0; i < pandas_geometry.size(); i++) {
					// (1) Translate panda
					panda = game.getPanda(i);
					pandaGeo = pandas_geometry.get(i);
					pandaGeo.setTranslation(panda.getAndSetNextPosition());

					// (2) Check collision of each panda to cage, if collision
					// detected,
					// add/remove score from player, delete pandas accordingly
					if (panda.isNearOrigin()) {
						// Increase Score
						game.addScore(0);
						setScoreText();
						
						// Remove Panda
						removePanda(i);
						game.refreshPandas();
					} else if (panda.isOutsideBoundary()) {
						// Decrease Score
						game.decScore(0);
						setScoreText();
						//						scoreView.setVisibility(View.VISIBLE);
						//System.out.println("Score: " + game.getScore(0));

						// Remove Panda
						removePanda(i);
						game.refreshPandas();
					}
				}
			} catch (Exception e) {
				MetaioDebug.log(Log.ERROR,
						"ARViewActivity.onDrawFrame: Rendering failed with error "
								+ e.getMessage());
			}
		}
	}
	
	public boolean checkGameEnd(){
		CharSequence s = timerTextView.getText();
		if(s.equals("Time Up!")){
			runOnUiThread(new Runnable() 
			{
				@Override
				public void run() 
				{
					String result = scoreView.getText().toString();
					String score = result.split(" ")[1];
					endGameMessageView.setText("Your score is " + score);
					endGameMenuLayout.setVisibility(View.VISIBLE);
				}
			});
			return true;
		}
		return false;
	}
	
	public void onBackMenuButtonClick(View v){
		Intent intent = new Intent(this, MainMenuActivity.class);
		// clear top is to clear the current activity in the stack and put mainMenuActivity into stack
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(intent);
	}
	public void setScoreText() {
		runOnUiThread(new Runnable() 
		{
			@Override
			public void run() 
			{
				scoreView.setText("score: " + game.getScore(0));
			}
		});
    }

	/**
	 * Called when the geometry(panda) is touched, panda will be rotated by 90
	 * degree when touched
	 * 
	 * @param geometry
	 *            geometry that to be rotated
	 */
	@Override
	protected void onGeometryTouched(IGeometry geometry) {
		// Check whether geometry is a panda
		if (pandas_geometry.contains(geometry)) {
			// Rotate panda
			geometry.setRotation(
					new Rotation(0.0f, 0.0f, -(float) Math.PI / 2), true);
			game.rotatePanda(pandas_geometry.indexOf(geometry));
		}
	}

	/**
	 * Remove panda from the list of pandas geometry
	 * 
	 * @param index
	 *            index of the panda in the list
	 */
	public void removePanda(int index) {
		pandas_geometry.get(index).setVisible(false);
		pandas_geometry.get(index).delete();
		pandas_geometry.remove(index);
		game.removePanda(index);
	}

	/**
	 * Create a new panda IGeometry object (with random position and direction),
	 * and add in to the array pandas_geometry
	 * 
	 * @param panda
	 *            - panda that needs to be rendered
	 */
	public void addPanda(Panda panda) {
		// Create panda geometry
		IGeometry mPanda = metaioSDK.createGeometry(PandaModelPath);
		if (mPanda != null) {
			mPanda.setScale(new Vector3d(PANDE_SCALE, PANDE_SCALE, PANDE_SCALE));
			mPanda.setTranslation(panda.getPosition());
			mPanda.setRotation(new Rotation(new Vector3d(0, 0, panda
					.getDirection().dot(new Vector3d(1, 0, 0)))), true);
			mPanda.setVisible(true);
			// If succeeds
			MetaioDebug.log("mPanda loaded!");
		} else {
			MetaioDebug.log("Error in loadding mPanda");
		}

		// Add panda to the pandas_geometry array
		this.pandas_geometry.add(mPanda);
	}
	
	@Override
	protected IMetaioSDKCallback getMetaioSDKCallbackHandler() 
	{
		return mCallbackHandler;
	}
	
	final class MetaioSDKCallbackHandler extends IMetaioSDKCallback 
	{

		@Override
		public void onSDKReady() 
		{
			// show GUI
			runOnUiThread(new Runnable() 
			{
				@Override
				public void run() 
				{
					mGUIView.setVisibility(View.VISIBLE);
					scoreView = (TextView) findViewById(R.id.arScoreView);
					timerTextView = (TextView) findViewById(R.id.arTimerView);
					pauseButton = (ImageButton) findViewById(R.id.pauseButton);
					endGameMenuLayout = (LinearLayout) findViewById(R.id.endGameBar);
					endGameMessageView = (TextView) findViewById(R.id.endGameMessageView);
					backToMenuButton = (Button) findViewById(R.id.backToMenuButton);
					counter = new Counter(TIME_LIMIT, TIME_UNIT, timerTextView);
					counter.start(); // start the counter
				}
			});
		}
	}
}