package edu.melbu.shinypandas.activities;

import java.io.IOException;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import com.metaio.sdk.MetaioDebug;
import com.metaio.tools.io.AssetsManager;

import edu.melbu.shinypandas.R;

public class MainActivity extends Activity {
	
	AssetsExtracter mTask;
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		mTask = new AssetsExtracter();
		mTask.execute(0);
		
		Intent intent = new Intent(getApplicationContext(), MainMenuActivity.class);
		startActivity(intent);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu){
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	/**
	 * AssetsExtractor is a AsyncTask that extracts all the assets in a background thread.
	 * It does not block the GUI.
	 */
	private class AssetsExtracter extends AsyncTask<Integer, Integer, Boolean> {
	  
        @Override
        protected Boolean doInBackground(Integer... params) 
        {
            try 
            {
                AssetsManager.extractAllAssets(getApplicationContext(), true);
            } 
            catch (IOException e) 
            {
                MetaioDebug.printStackTrace(Log.ERROR, e);
                return false;
            }    
            return true;
        }    
    } 
}
